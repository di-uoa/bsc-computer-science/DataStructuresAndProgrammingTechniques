#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* function prototypes */
void InsertNewSecondNode(NodeType **);
void InsertNewLastNode(char *, NodeType **);
void DeleteLastNode(NodeType **);
NodeType *ListSearch(char *, NodeType *);
void PrintList(NodeType *);
void InsertNewFirstNode(char *A, NodeType **L);

int LengthCounter (NodeType *L);
NodeType *Select (NodeType **L, int i);
void Replace (NodeType **L, int i, char *A);
