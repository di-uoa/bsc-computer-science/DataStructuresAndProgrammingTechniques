#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef char NodeData [4];
typedef struct NodeTag {
                        NodeData Data;
                        struct NodeTag *Link;
                } NodeType;
typedef NodeType *NodePointer;

void InsertNewLastNode(char *, NodeType **);
int LengthCounter (NodeType *L);

int main(void)
{
    NodeType *L;
    int length = 0;

    L=NULL;
    InsertNewLastNode("EL1", &L);  //inserting nodes to the list
    InsertNewLastNode("EL2", &L);
    InsertNewLastNode("El3", &L);

    length = LengthCounter(L);//count the length of the list
    printf("The length of the list is: %d\n", length);//print the length of the list
}

void InsertNewLastNode(char *A, NodeType **L)
{
    NodeType *N, *P;

    N=(NodeType *)malloc(sizeof(NodeType));
    strcpy(N->Data, A);
    N->Link=NULL;

    if (*L == NULL) {
        *L=N;
    } else {
        P=*L;
        while (P->Link != NULL) P=P->Link;
        P->Link=N;
    }

}

int LengthCounter (NodeType *L)  //this is the function which calculates the length of the list
{
    NodeType *N=L;
    //printf("Node: %s : ", N->Data);
    if (N == NULL) return -1;
    if (N->Link == NULL)  //if the list has only one node
    {
        //printf("has NULL link!\n");
        return 1;  //base case

    }else{
        //printf("has not a NULL link!\n");
        return 1 + LengthCounter(N->Link); //we call again the function for this smaller problem
    }

}
        
      
