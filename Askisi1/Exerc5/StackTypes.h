/* This is the file StackTypes.h   */
typedef struct Position {
    int x;
    int y;
}Position;

typedef Position ItemType;
/* char is the type for our first application */
/* float is the type for our second application */

typedef struct StackNodeTag {
           ItemType Item;
           struct StackNodeTag *pathLink;
           struct StackNodeTag *Link;
        } StackNode;

typedef struct {
           StackNode *ItemList;
        } Stack;
