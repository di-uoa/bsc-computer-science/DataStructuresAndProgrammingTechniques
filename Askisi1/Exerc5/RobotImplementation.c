#include "RobotInterface.h"

char **createMap (int rows, int columns, int obstaclesPercent)
{
    //create an empty map
    char **Map;
    Map = (char**) malloc(rows * sizeof(char*));

    int i, j;
    for(i=0;i<columns;++i)
        Map[i] = (char*) malloc(columns * sizeof(char));
    //randomly put obstacles and spaces with the given probability
    for(i=0;i<rows;i++)
    {
        for(j=0;j<columns;j++)
        {
            int r = rand()%101; //using the rand function a radom number is generated
            if(r<obstaclesPercent) //if the given by the user possibility is greater than the generated number I fill the map with opstacle (#)
            {
                Map[i][j] = OBSTACLE;
            }
            else //otherwise it's an empty place
            {
                Map[i][j] = SPACE;
            }
        }
    }

    return Map; //the function returns the created map
}
void PrintPosition(Position a) //a function which prints a position from the map
{
    printf("(%d,%d)",a.x,a.y);
}

void PrintMap (char **Map, int rows, int columns) //this function prints the map
{
    int i,j;
    printf("\n");
    for (i=0; i<rows; i++)
    {
        for(j=0; j<columns; j++)
        {
            printf("%c", Map[i][j]);
        }
        printf("\n");
    }
}

Position findNextPosition (char **Map, Position current, int rows, int columns)
{
    Position left;
    left.x = current.x; left.y = current.y - 1; //this is the current's left position
    if(left.y >=0)
    {
        if (Map[left.x][left.y] == SPACE || Map[left.x][left.y] == GOAL) return left; //if this position is space or the goal state the function returns its value
    }
    //with the same way, we can find the right, the up and the down position
    Position right;
    right.x = current.x; right.y = current.y +1;
    if(right.y<columns)
    {
        if(Map[right.x][right.y] == SPACE || Map[right.x][right.y] == GOAL) return right;
    }

    Position up;
    up.x = current.x - 1; up.y = current.y;
    if(up.x>=0)
    {
        if (Map[up.x][up.y] == SPACE || Map[up.x][up.y] == GOAL) return up;
    }

    Position down;
    down.x = current.x + 1; down.y = current.y;
    if(down.x<rows)
    {
        if (Map[down.x][down.y] == SPACE || Map[down.x][down.y] == GOAL) return down;
    }
    return current; //the function returs either the old current position, if there isn't any other position (space or goal) or the new current position
}
int equals(Position a, Position b) //this function checks if two position are equals
{
    return (a.x == b.x && a.y == b.y);
}

void CreatePath (Position current, Stack *VisitedPositions, char **Map, int rows, int columns)
{

    Map[current.x][current.y] = PATH; //the current position isn't in the stack so I mark it as PATH

    StackNode * node = VisitedPositions->ItemList;

    while (node !=NULL) //until the positions in the stack are finished, I mark these positions as PATH on the map
    {
        int x = node->Item.x;
        int y = node->Item.y;
        Map[x][y] = PATH;
        node = node ->pathLink;
    }

}

