#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "QueueInterface.h"


int main(int argc, char ** argv)
{

    Queue myQueue;
    ItemType i;
    InitializeQueue(&myQueue); // pass Queue** to preserve changes

    char stopChar[] = "#";
    printf("Give an 4-character element for the queue. To stop inserting elements give %s",stopChar);
    scanf("%s", i);
    while (strcmp(i,stopChar) != 0) { // until the user inserts the character: #
        printf("Adding element %s to the queue\n",i);

        Insert(i,&myQueue); //inserting new elements to the queue
        printf("Queue is now:\n");
        PrintQueue(&myQueue);
        printf("Give an 4-character element for the queue. To stop inserting elements give %s",stopChar);
        scanf("%s", i); //user gives new character
    }
    printf("Total elements inserted:\n");
    PrintQueue(&myQueue);

    //remove function if the user wants the first element to be removed
    int numRemove,j;
    printf("Do you want to remove any elements? if so, how many?\n");
    scanf("%d",&numRemove);
    for(int j=0;j<numRemove;++j)
    {
        ItemType item;
        Remove(&myQueue,&item);
        printf("Removed element %s\n",item);
    }
    PrintQueue(&myQueue);
    printf("Exiting.");
    return 1;
}
