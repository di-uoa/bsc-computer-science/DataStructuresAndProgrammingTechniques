/* This is the file QueueTypes.h */

typedef char ItemType[4];
/* the item type can be arbitrary */

typedef struct Node {
                     ItemType Item;
                     struct Node *Link;
                } Node;

typedef struct {
                    Node *stackQueue,*stackHelper;
               } Queue;
