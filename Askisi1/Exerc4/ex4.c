#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "StackInterface.h"

int main (int argc,char ** argv)
{
    Stack *S;
    InitializeStack(S);
    // create a file pointer, open the file
    FILE * filePointer;
    if(openFile(&filePointer) <0) return -1;

    // use getline() to read the file line by line
    size_t numCharsRead, len=0;

    char * line = NULL;

    while ((numCharsRead = getline(&line, &len, filePointer)) != -1)
    {
        // delete the newline
        line[numCharsRead-1]='\0';
        printf("\nThe next line is: [%s] \n", line);


        // the line that has been processed
        char processedLine[numCharsRead];
        int processedLineLength;
        // convert the line , and get its length
        convertLine(line,processedLine,numCharsRead,&processedLineLength);
        CheckForPalindrome(S, processedLine, processedLineLength);
    }
    // file was read!
    fclose(filePointer);
}
