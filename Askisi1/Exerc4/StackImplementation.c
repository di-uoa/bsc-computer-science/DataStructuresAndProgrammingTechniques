/* This is the file StackImplementation.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "StackInterface.h"

void InitializeStack(Stack *S)
{
   S->ItemList=NULL;
}

int Empty(Stack *S)
{
   return (S->ItemList==NULL);
}

int Full(Stack *S)
{
  return 0;
}
/* We assume an already constructed stack is not full since it can potentially */
/* grow as a linked structure  */

void Push(ItemType X, Stack *S)
{
   StackNode *Temp;

   Temp=(StackNode *) malloc(sizeof(StackNode));

   if (Temp==NULL){
      printf("system storage is exhausted");
   } else {
      Temp->Link=S->ItemList;
      Temp->Item=X;
      S->ItemList=Temp;
   }
}


void Pop(Stack *S, ItemType *X)
{
   StackNode *Temp;
    if (X == NULL)
    {
        printf("Container supplied Pop is NULL!\n");
        return;
    }
   if (S->ItemList==NULL){
     printf("Attempted to pop a NULL stack itemlist\n");
   }
   else if(S == NULL)
   {
       printf("Attempted to pop a NULL stack\n");

   }
   else {
      Temp=S->ItemList;
      *X=Temp->Item;
      S->ItemList=Temp->Link;
      free(Temp);
   }
}

void PrintStack(Stack *S)//function for printing the Stack
{

    StackNode *N;

      printf("(");
      N = S->ItemList;
      while(N != NULL) {
         printf("%c", N->Item);

         N=N->Link;
         if (N!=NULL) printf(",");
      }
      printf(")\n");
}

int openFile(FILE **fp)
{
    *fp = fopen("./palindromes.txt", "r");
    // check that the file opened succcessfully
    if(*fp == NULL) {
        printf ("The file couldn't be opened!");
        return -1;
    }
    return 1;
}

void convertLine(char * line, char *processedLine,int lineLength,int *processedLineLength)
{
    int i;
    int j = 0;
    for (i=0;i<lineLength;i++)
    {
        //if it's uppercase make it lowercase
        if (line[i]>64 && line[i]<91)
        {
            processedLine[j++] = line[i]+32;
        }else if (line[i]>96 && line[i]<123)//if it's lowercase it's fine
        {
            processedLine[j++] = line[i];
        }else { //anything else we ignore it
            continue;
        }
    }
    processedLine[j] = '\0';
    *processedLineLength = j;
}

void CheckForPalindrome(Stack *S, char *processedLine, int processedLineLength)
{
    int i;

    printf("The processed line is: [%s]. Line length is %d \n", processedLine,processedLineLength);
    ItemType PoppedElement;
    //for(i=0;i<processedLineLength;i++) printf("%d - %c\n",i,processedLine[i]);

    // Loop over the number of characters in the processed sentence
    for(i=0;i<processedLineLength;i++)
    {
        // Case where the line has an odd number of characters
        //if (i == 0) printf("the processed line has an odd number of characters : %d\n",processedLineLength);
        if(processedLineLength%2 == 1)
        {
            if(i<processedLineLength/2)//we push into the stack the first half of the phrase
            {
                //printf("We at character #%d.Pushing %c to the stack\n",i,processedLine[i]);
                Push(processedLine[i], S);
            }
            else if(i == (processedLineLength/2))//we print the letter that has to be checked with itself
            {
                //printf("We at character #%d, which is %c, the middle of sentence with an odd number of characters.We do nothing.\n",i,processedLine[i]);

                continue;
            }
            else
            {//now we pop all the elements and check it with the next letters of the phrase
                   // printf("We at character #%d, and we 're past the middle.We'll pop from the stack and compare the value we get with the current character, %c\n",i,processedLine[i]);
                    Pop(S,&PoppedElement);
                    if(PoppedElement != processedLine[i])
                    {
                        printf("Character mismatch: %c %c at positions %d , %d \n", PoppedElement, processedLine[i],i,processedLineLength-i-1);
                        break;
                    }

            }
        }//end of odd number of letter's case
        //now we do the same thing for the case with line has even number of letters
        else
        {

            //if (i == 0) printf("the processed line has an even number of characters : %d\n",processedLineLength);
            if(i<processedLineLength/2) Push(processedLine[i], S); //the first half of the phrase we push it to the stack
            else //we start comparing the characters of the line
            {
                Pop(S, &PoppedElement);
                if(PoppedElement != processedLine[i])
                {
                    printf("Character mismatch: %c %c at positions %d , %d \n", PoppedElement, processedLine[i],i,processedLineLength-i-1);
                    break;
                }
            }//end of comparing

        }//end of even number of letter's case
    }//we have read the line

    //result of the comparison

    if(i >= processedLineLength)
        printf("The string [%s] is a palindrome!\n",processedLine);
    else
    {
        printf("The string [%s] is NOT palindrome!\n",processedLine);
        while(!Empty(S))
        {
            ItemType X;
            Pop(S,&X);
        }
     }


}

