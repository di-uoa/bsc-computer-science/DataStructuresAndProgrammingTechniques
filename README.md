# Data Structures And Programming Techniques

ReadMe μόνο για τα προγραμματιστικά ερωτήματα

## Άσκηση 1

### Ερώτημα 1

Χρησιμοποίησα όπως ζητήθηκε τα typedefs για τη συνδεδεμένη λίστα όπως παρουσιάστηκαν στο μάθημα και τα προσάρμοσα στο 
συγκεκριμένο πρόβλημα.Από κάτω όρισα τα πρωτότυπα των δυο συναρτήσεων που χρησιμοποίησα. Η μια συνάρτηση είναι η 
InsertNewLastNode, η οποία εισάγει ένα στοιχείο στο τέλος της λίστας. Η δεύτερη συνάρτηση υπολογίζει το μήκος της λίστας 
(LengthCounter). Η υλοποίηση έγινε αναδρομικά, όπως ζητήθηκε. Αρχικά, ορίζω έναν νέο pointer τύπου NodeType που δείχνει στην 
ίδια θέση που δείχνει ο pointer L, στην αρχή της λίστας. Γίνεται έλεγχος για την περίπτωση που η λίστα είναι κενή και 
επιστρέφει την τιμή -1. Στη συνέχεια, ελέγχω αν η λίστα έχει μόνο ένα στοιχείο ή έχει μείνει μόνο ένα στοιχείο να προστεθεί 
στο μήκος (base case) επιστρέφει 1, αλλιώς επιστρέφει 1 + την τιμή που θα επιστρέψει αναδρομικά η συνάρτηση που την ξανακαλώ 
για το επόμενο στοιχείο. Τέλος, έγραψα την κατάλληλη main που καλεί τις δυο συναρτήσεις και αφού υπολογίσει το μήκος της 
λίστας, εκτυπώνει το μήκος.

### Ερώτημα 2

Στο 2ο ερώτημα χρησιμοποίησα τον κώδικα από τη συνδεδεμένη λίστα για τα αεροδρόμια (typedefs, prototypes) καθώς επίσης και 
τις υλοποιήσεις των συναρτήσεων. Επίσης, χρησιμοποίησα τη συνάρτηση του ερωτήματος 1 (LengthCounter). Όλα τα typedefs 
βρίσκονται στο αρχείο ListTypes.h, όλα τα πρωτότυπα των συναρτήσεων βρίσκονται στο αρχείο InterfaceList.h και οι υλοποιήσεις 
όλων των συναρτήσεων βρίσκονται στο αρχείο ListImplementation.c.

H πρώτη συνάρτηση είναι η Select. Όπως και στην LengthCounter ορίζω έναν pointer N που τον βάζω να δείχνει στην αρχή της 
λίστας, δηλαδή στη διεύθυνση που δείχνει η L. Καλώ τη συνάρτηση LengthCounter για να υπολογίσω το μήκος της λίστας. Στη 
συνέχεια ελέγχω αν ο ακέραιος i είναι μεταξύ του 0 και του μήκους της λίστας. Αν δεν είναι τότε η συνάρτηση επιστρέφει NULL. 
Μετά ορίζω μια μεταβλητή int, την count, που την αρχικοποιώ στο μηδέν. Στη συνέχεια, με ένα while, που ισχύει μέχρι να πάρει 
ο count την τιμή του i, αυξάνω τον count κατά 1 και διατρέχω τη λίστα μέχρι να βρω το στοιχείο. Αφού το βρει επιστρέφει τον 
pointer που πλέον δείχνει σε αυτό το στοιχείο.

Η δεύτερη συνάρτηση είναι η Replace, που παίρνει σαν ορίσματα τη λίστα, έναν ακέραιο i για να αντικαταστήσουμε το iοστο 
στοιχείο της λίστας και το νέο στοιχείο που θέλουμε να βάλουμε στη θέση του. Αρχικά, καλώ την select, η οποία μου επιστρέφει 
έναν pointer που δείχνει στο iοστο στοιχείο. Υπάρχουν δυο τρόποι να υλοποιήσουμε την Replace. Ορίζω πάλι έναν pointer που τον 
βάζω να δείχνει στο νέο στοιχείο που θέλω να προσθέσω (κάνω string copy στο πεδίο airport το όνομα που έχω περάσει ως όρισμα) 
και ορίζω επίσης και δυο ακόμα pointers που δείχνουν στον προηγούμενο και στον current κόμβο. Ο πρώτος έλεγχος είναι για 
κενή λίστα, που τότε η συνάρτηση απλώς επιστρέφει χωρίς να κάνει τίποτα. Ο επόμενος έλεγχος είναι για το αν η λίστα έχει ένα 
μόνο κόμβο ο οποίος ελευθερώνεται και τώρα ο pointer L δείχνει εκεί που έδειχνε ο Ν, αλλιώς ορίζω τον PreviousNode να δείχνει 
στον πρώτο κόμβο της λίστας,παίρνοντας την διεύθυνση που δείχνει ο *L και τον CurrentNode να δείχνει στον δεύτερο κόμβο 
(εκεί που δείχνει το link του πρώτου κόμβου). Με τη βοήθεια αυτών των δυο pointers διατρέχω τη λίστα μέχρι ο CurrentNode να 
δείχνει στην ίδια θέση με τον item. Έτσι, συνδέω το νέο στοιχείο στη λίστα και ελευθερώνω το στοιχείο προς αντικατάσταση. 
Διαφορετικά, θα μπορούσα απλώς να αντιγράψω στο πεδίο Airport τα νέα δεδομένα χρησιμοποιώντας τον pointer που επιστρέφει η 
select (υπάρχει η υλοποίηση σε σχόλια).

### Ερώτημα 3

Η άσκηση αυτή ζητάει να υλοποιηθεί μια ουρά με τη χρήση 2 στοιβών. Επομένως, για να μπορέσουμε να έχουμε δομή FIFO, κάθε φορά 
που ο χρήστης θα δίνει ένα νέο στοιχείο, πρέπει να ελέγχουμε τη στοίβα που χρησιμεύει ως ουρά (stackQueue) αν είναι άδεια, 
και αν όχι να αφαιρούμε όλα τα στοιχεία και να τα βάζουμε στην βοηθητική στοίβα (stackHelper), να εισάγω το νέο στοιχείο στην 
stackQueue και στη συνέχεια να ξαναβάζω πίσω τα στοιχεία που είχα βάλει στην stackHelper.

Ας πάρουμε ένα παράδειγμα.

Έστω ότι φτάνουν με την ακόλουθη σειρά τα στοιχεία: a, b, c, d, #. Φτάνει πρώτα το a. Η stackQueue είναι κενή επομένως 
γίνεται  απλώς push το a στην stackQueue. Στη συνέχεια, ο χρήστης εισάγει το b. Γίνεται pop το a από τη stackQueue και push 
στην stackHelper, εισάγω το στοιχείο b στην stackQueue και τέλος κάνω pop το a από τη stackHelper και push στην stackQueue. 
Οπότε στην stackQueue το πάνω πάνω στοιχείο (που θα γίνει πρώτο pop) είναι το a που είχε φτάσει πρώτο (FIFO). Με τον ίδιο 
τρόπο συνεχίζουμε για τα υπόλοιπα στοιχεία. Αφού λάβουμε το #, εκτυπώνουμε την ουρά.

Επομένως, για την άσκηση αυτή πήρα τον κώδικα για τις ουρές και τον τροποποίησα ώστε να υλοποιείται με στοίβες. Στο αρχείο 
QueueInterface.h υπάρχουν τα πρωτότυπα όλων των συναρτήσεων που χρησιμοποίησα. Η υλοποίηση αυτών των συναρτήσεων είναι στο 
αρχείο QueueImplementation.c και τροποποιήθηκαν κατάλληλα ώστε να υλοποιούν στοίβες. Οι συναρτήσεις που θα υλοποιηθούν είναι 
η InitializeQueue, που αρχικοποιεί την ουρά, δηλαδή τις δυο στοίβες ως άδειες, η Empty που ελέγχει αν η δομή είναι άδεια, η 
Insert που χρησιμοποιείται από την ουρά για να εισάγει ένα στοιχείο, η Remove, που επίσης χρησιμοποιείται από την ουρά για να 
αφαιρέσει ένα στοιχείο, η Push που χρησιμοποιείται από τη στοίβα για να μπει ένα στοιχείο στη δομή και η Pop για να αφαιρεθεί 
ένα στοιχείο από τη στοίβα. Επίσης, για μεγαλύτερη ευκολία χρησιμοποιήθηκαν και η PrintLinkedList και η PrintQueue. 

Στο αρχείο QueueImplementation.c υπάρχει ο κώδικας για τις υλοποιήσεις όλων των συναρτήσεων. Η IntializeQueue αρχικοποιεί τις 
δυο στοίβες. Η Empty κάνει τον έλεγχο για το αν η στοίβα είναι κενή. Η Insert δέχεται σαν όρισμα το στοιχείο που πρέπει να 
προστεθεί στην ουρά και έναν pointer σε ουρά και για όσο η στοίβα που χρησιμεύει ως ουρά δεν είναι άδεια κάνει Pop όλα τα 
στοιχεία της stackQueue και τα κάνει Push στην stackHelper. Στη συνέχεια κάνει Push στην stackQueue το νέο στοιχείο και τέλος 
μέχρι να αδειάσει η stackHelper κάνει Pop τα στοιχεία από εκεί και τα κάνει Push στην stackQueue. Η Remove απλώς βγάζει το 
πάνω πάνω στοιχείο της stackQueue. Οι Push και Pop συναρτήσεις είναι οι αντίστοιχες υλοποιήσεις σε στοίβα για την εισαγωγή 
και την αφαίρεση στοιχείων (όπως δόθηκαν στο μάθημα για τις στοίβες. Τέλος, οι δύο συναρτήσεις για την εκτύπωση της 
συνδεδεμένης λίστας και της ουράς, που χρησιμοποιούνται για συντομία.

Στο αρχείο ex3.c, στην main, δημιουργώ μια ουρά που την κάνω Initialize και μια μεταβλητή τύπου ItemType. Ορίζω ποιος είναι 
ο χαρακτήρας με τον οποίο θα σταματάει ο χρήστης να εισάγει στοιχεία στην ουρά (#). Στη συνέχεια, ζητάω από τον χρήστη να μου 
δώσει κάποιο στοιχείο και το αποθηκεύω στην μεταβλητή i. Μέχρι ο χρήστης να εισάγει τη #, καλώ την Insert για να βάλω στο 
κάτω μέρος της stackQueue το νέο στοιχείο και με τη βοήθεια της stackHelper να μπούνε και τα υπόλοιπα στοιχεία με τη σειρά 
που έφτασαν (πρώτο το πρώτο στοιχείο που έχει δώσει ο χρήστης, δεύτερο το δεύτερο κλπ), εκτυπώνω την ουρά και ζητάω από το
χρήστη να μου δώσει ένα επόμενο στοιχείο για την ουρά. Όταν ο χρήστης τελειώσει την εισαγωγή στοιχείων εκτυπώνω τον αριθμό 
των στοιχείων που έδωσε και την ουρά. Τέλος, τον ρωτάει αν θέλει να βγάλει στοιχεία και πόσα, παίρνω ως είσοδο τον αριθμό που 
θα μου δώσει και κάνω Remove τα/το στοιχεία/στοιχείο που μου έχει πει ο χρήστης και εκτυπώνω εκ νέου την ουρά.

### Ερώτημα 4

Στο ερώτημα αυτό χρησιμοποίησα την υλοποίηση στοίβας όπως υπάρχει στο site του μαθήματος υλοποιώντας τις απαραίτητες 
τροποποιήσεις. Στο αρχείο StackTypes.h υπάρχουν όλα τα typedefs και τα structs όπως δόθηκαν στο μάθημα, και στο 
StackInterface.h τα πρωτότυπα όσων συναρτήσεων χρησιμοποιούνται. Στο αρχείο StackImplementation.c χρησιμοποιώ τις συναρτήσεις 
που δόθηκαν στο μάθημα (InitializeStack, Empty, Full, Push, Pop, PrintStack) και στη συνέχεια έφτιαξα ορισμένες ακόμα για την 
υλοποίηση του προβλήματος.

Την openFile για να ανοίξω το αρχείο με τις φράσεις προς έλεγχο και να ελέγξω ότι άνοιξε σωστά. Την convertLine, που παίρνει 
σαν όρισμα την εκάστοτε γραμμή, το μήκος της γραμμής, και δυο pointers, έναν που θα αποθηκεύσει την αλλαγμένη συμβολοσειρά 
και έναν για το νέο μήκος της. Στη συνέχεια διατρέχει όλη τη γραμμή και αν βρει κεφαλαίο γράμμα το μετατρέπει σε μικρό (με 
βάση τις τιμές που έχουν στον πίνακα ASCII), εάν βρει πεζά γράμματα τα αφήνει ως έχουν και οτιδήποτε άλλο το αγνοεί και 
υπολογίζει το νέο μήκος της γραμμής. Η επόμενη συνάρτηση είναι η CheckForPalindrome, που παίρνει σαν όρισμα τη στοίβα, την 
τροποποιημένη φράση και το νέο μήκος όπως υπολογίστηκαν από την convertLine, και στη συνέχεια με το for διατρέχει όλη τη 
γραμμή, κάνει έλεγχο ότι η γραμμή έχει γράμματα, και διακρίνει δυο περιπτώσεις. Η πρώτη περίπτωση είναι όταν το μήκος
είναι μονός αριθμός: για τα i που είναι μικρότερα απ το processedLineLength/2 (ακέραια διαίρεση) τα κάνουμε push στη στοίβα, 
το στοιχείο που είναι ίσο με processedLineLength/2 απλώς το προσπερνάω χωρίς να κάνω τίποτα και για τα υπόλοιπα κάνω pop τα 
στοιχεία που ήταν στη στοίβα και τα συγκρίνω με τα άλλα μισά. Αν είναι όλα ίδια τότε η φράση είναι palindrome. Η δεύτερη 
περίπτωση είναι για φράσεις με ζυγό αριθμό γραμμάτων. Ομοίως υλοποιείται και αυτό απλώς δεν υπάρχει στοιχείο στη μέση που 
απλώς να χρειάζεται να το εκτυπώσω αλλά συγκρίνω το πρώτο μισό της φράσης με το υπόλοιπο μισό. Στο τέλος, αδειάζω τη
στοίβα (για την περίπτωση που δεν έχω palindrome) και προχωράω στην επόμενη γραμμή.

Τέλος, είναι το αρχείο ex4.c, όπου η main κάνει αρχικά initialize τη στοίβα, στη συνέχεια δημιουργεί έναν filePointer και 
καλεί την openFile για να ανοίξει το αρχείο, ελέγχει αν άνοιξε σωστά και αν όχι επιστρέφει. Αν το αρχείο άνοιξε, καλώ την 
getline για να διαβάσω το αρχείο γραμμή-γραμμή και όσο υπάρχουν γραμμές προς διάβασμα, εκτυπώνω τη νέα σειρά και στη 
συνέχεια καλώ πρώτα την convertLine και μετά την CheckForPalindrome για να ελέγξω γραμμή προς γραμμή ποιες φράσεις είναι 
παλίνδρομα. Τέλος, κλείνω το αρχείο.

### Ερώτημα 5

Στην άσκηση αυτή τα αρχεία που περιλαμβάνονται είναι: StackTypes.h, StackInterface.h, RobotInterface.h, StackImplementation.c, 
RobotImplementation.c και ex5.c. Τα αρχεία StackTypes.h, StackInterface.h και StackImplementation.c αφορούν την υλοποίηση της 
στοίβας και είναι ο κώδικας που δόθηκε στο μάθημα. Στο αρχείο RobotInterface.h βρίσκονται όλα τα typedefs, τα defines και τα
πρωτότυπα των συναρτήσεων που χρησιμοποίησα για την υλοποίηση της άσκησης.

Στο RobotImplementation.c αρχικά έχω φτιάξει μια συνάρτηση την createMap, η οποία με τη βοήθεια της rand(), δημιουργεί με 
βάση μια δοσμένη πιθανότητα από τον χρήστη εμπόδια και κενές θέσεις σε ένα χάρτη του οποίου το μέγεθος καθορίζεται επίσης 
από τον χρήστη (στη main). Οι δυο επόμενες συναρτήσεις είναι βοηθητικές, η μια τυπώνει μια θέση στον χάρτη και η άλλη τυπώνει 
τον χάρτη. Στη συνέχεια, είναι η findNextPosition που παίρνει ως ορίσματα τον χάρτη, την τρέχουσα θέση και τον αριθμό των 
γραμμών και στηλών του πίνακα που περιέχει τον χάρτη και αναζητά στις θέσεις γύρω της (πάνω, κάτω, δεξιά και αριστερά) αν
υπάρχει κάποια κενή θέση. Αν βρει κάποια τη βάζει στην current και την επιστρέφει. Μια ακόμα βοηθητική συνάρτηση η equals 
που ελέγχει αν δυο θέσεις έχουν ίδιες "συντεταγμένες". Τέλος, είναι η CreatePath η οποία αρχικά μαρκάρει ως μέρος του 
μονοπατιού την current position καθώς αυτή δεν βρίσκεται στη στοίβα, και στη συνέχεια διατρέχει τη στοίβα και μαρκάρει όσες 
θέσεις βρίσκονται εκεί ως PATH, για να φαίνεται ευδιάκριτα στο χάρτη το μονοπάτι.

Στο ex5.c, στη main, ζητάει από τον χρήστη είτε να βάλει ως ορίσματα όταν εκτελεί το αρχείο στο terminal τις γραμμές, τις 
στήλες του πίνακα και την πιθανότητα εμφάνισης εμποδίων είτε να τα εισάγει μετά και καλεί την createMap με αυτά τα
ορίσματα για να δημιουργήσει ένα χάρτη, στον οποίο τοποθετεί με τυχαίο τρόπο με τη βοήθεια της rand() μια θέση Start S και 
μια θέση Goal G. Στη συνέχεια, δημιουργεί μια στοίβα όπου τοποθετεί τις θέσεις από τις οποίες έχει περάσει το robot, 
δημιουργεί τις θέσεις start, goal και αρχικοποιεί την current στην start position. Μέχρι να βρεθεί η θέση στόχος τυπώνει τον 
χάρτη, καλεί την findNextPosition για να επιλέξει την επόμενη θέση και, αν η next επιστρέψει την ίδια τιμή με την current 
σημαίνει πως δεν υπάρχει κάποια κενή θέση γύρω της και ελέγχει αν η στοίβα είναι άδεια (αν είναι σημαίνει πως δεν υπάρχει 
μονοπάτι και επιστρέφει) και αν δεν είναι, μαρκάρω την τωρινή θέση ως visited, κάνω pop την προηγούμενη θέση, την οποία 
ορίζω ως current και αναζητά στις δικές της γειτονικές θέσεις για κάποια άλλη κενή. Αν η next επιστρέψει με νέα τιμή, 
μαρκάρουμε τη θέση ως visited, τη βάζουμε στη στοίβα και δίνουμε στην current την τιμή της next που επιστράφηκε από την 
findNextPosition. Την current θέση την μαρκάρω με ένα c στο χάρτη για να είναι πιο ευδιάκριτη. Τέλος, καλώ την CreatePath 
για να μαρκάρει ευδιάκριτα το μονοπάτι στον χάρτη, φτιάχνω ξανά τις start και goal positions για να φαίνεται καθαρά από πού 
ξεκίνησε το ρομπότ και πού ήταν η θέση στόχος και εκτυπώνω το χάρτι με το μονοπάτι πλέον με τη βοήθεια της PrintMap.

## Άσκηση 2

### Ερώτημα 7

Η συνάρτηση Insert Tree παίρνει σαν όρισμα τη ρίζα του δέντρου και τον νέο κόμβο που θέλουμε να εισάγουμε. Βοηθητικά 
χρησιμοποιώ δυο pointers σε TreeNode, CurrentNode και PreviousNode. Αρχικά, ο CurrentNode δείχνει στη ρίζα και ο PreviousNode 
είναι NULL. Επίσης, χρησιμοποιώ και μια μεταβλητή, την right, που παίρνει τιμή είτε 0 είτε 1 για να ξέρουμε αν ο νέος κόμβος 
θα εισαχθεί δεξιά ή αριστερά όταν το CurrentNode γίνει NULL. Στη συνέχεια έχουμε ένα while loop όπου κάθε φορά που μπαίνουμε 
αρχικοποιούμε την right στο 0 και ανάλογα αν ο νέος κόμβος είναι μικρότερος από τον τρέχοντα κόμβο πάμε δεξιά ή αριστερά και
βάζουμε τον PreviousNode να δείχνει στον τρέχοντα κόμβο και τον CurrentNode στο δεξί ή αριστερό παιδί του αντίστοιχα. Στην 
περίπτωση που πάμε δεξιά κάνουμε το right 1. Αφού βγούμε από το while loop βάζουμε στον CurrentNode να δείχνει στον νέο κόμβο 
που θέλουμε να εισάγουμε και κάνουμε τους δείκτες που δείχνουν στα παιδιά του NULL. Ελέγχουμε αν ο PreviousNode είναι NULL, 
που σημαίνει ότι δεν έχουμε μπει καμία φορά στο while loop, επομένως το δέντρο είναι κενό και βάζουμε τη ρίζα να δείχνει στον 
CurrentNode, αλλιώς αν η μεταβλητή right είναι 1 βάζουμε το δεξί δείκτη του PreviousNode να δείχνει στον CurrentNode αλλιώς
βάζουμε τον αριστερό και στο τέλος επιστρέφουμε τη ρίζα.

### Ερώτημα 8

Η συνάρτηση DeleteKeyTree παίρνει σαν όρισμα την ρίζα ενός δέντρο και το κλειδί που θέλει να διαγράψει και ελέγχει αν ο 
τρέχων κόμβος είναι NULL οπότε εκτυπώνει πως δεν υπάρχει κόμβος με αυτό το κλειδί στο δέντρο, αλλιώς αν το κλειδί βρίσκεται 
στον τρέχων κόμβο τον διαγράφει με την συνάρτηση DeleteNodeTree, την οποία πρέπει να αλλάξουμε, αλλιώς αν το κλειδί είναι 
μικρότερο από τον τρέχων κόμβο πάμε αριστερά και καλούμε αναδρομικά την συνάρτηση αλλιώς αν είναι μεγαλύτερο πάμε δεξιά και 
την καλούμε αναδρομικά.

Η DeleteNodeTree παίρνει σαν όρισμα ένα δείκτη στον κόμβο που θέλουμε να διαγράψουμε. Δημιουργούμε έναν pointer που δείχνει 
στον ίδιο κόμβο και ελέγχουμε αν είναι NULL οπότε ο κόμβος δεν υπάρχει αλλιώς αν δεν υπάρχει δεξί παιδί βάζουμε το parentLink 
να δείχνει στο αριστερό και ελευθερώνουμε τον κόμβο μέσω του δείκτη r αλλιώς αν δεν υπάρχει αριστερό παιδί κάνουμε ό,τι
προηγουμένως για το δεξί παιδί αυτή τη φορά. Η τελευταία περίπτωση που ελέγχεται είναι το να υπάρχουν και τα δυο παιδιά, και 
είναι το κομμάτι που έπρεπε να αλλάξουμε. Δημιουργούμε δυο pointers σε TreeNode, τον q και τον PreviousNode ο οποίος αρχικά 
δείχνει στον ίδιο κόμβο με το parentLink ενώ ο q πάει στο δεξί υποδέντρο. Έχουμε και μια μεταβλητή childq που αρχικά είναι 
μηδέν και τη χρειαζόμαστε για να ελέγξουμε ότι θα πάμε τουλάχιστον μια φορά αριστερά.

Ξεκινάμε να διατρέχουμε προς τα αριστερά το δεξί υποδέντρο για να βρούμε τον αμέσως επόμενο του κόμβου που θέλουμε να 
διαγράψουμε. Κάνουμε το childq 1 για να δείξουμε ότι πήγαμε σίγουρα μια φορά αριστερά, το PreviousNode το κάνουμε q και το q 
συνεχίζει να διασχίζει το υποδέντρο προς τα αριστερά. Βγαίνοντας από το while loop ελέγχουμε αν το childq είναι 1 και αν 
είναι, βάζουμε στο πεδίο entry του parentLink το entry του q (που είναι το κλειδί με την αμέσως μεγαλύτερη τιμή του) και 
κάνουμε reattach το δεξί υποδέντρο του q ως αριστερό παιδί του PreviousNode, αλλιώς, αντιγράφουμε και πάλι το πεδίο entry 
όπως στην 1η περίπτωση αλλά κάνουμε reattch το δεξί υποδέντρο του q ως δεξί παιδί, αυτή τη φορά, στο PreviousNode, και τέλος 
κάνουμε free το q.

Έστω ότι έχουμε τις τιμές: 15,10, 18, 16, 17,8,13. Το δέντρο θα είναι όπως φαίνεται στο παρακάτω σχήμα. Έστω ότι αρχικά 
θέλουμε να διαγράψουμε τον κόμβο 15 και θα πάμε πρώτα δεξιά στον κόμβο 18 και τέρμα αριστερά που είναι ο κόμβος 16. Θα
αντικαταστήσουμε την τιμή του 15 με το 16 και θα σβήσουμε τον κόμβο που ήταν το 16 και έχει μόνο 1 παιδί, το 17. Στην 
περίπτωση που έχουμε πάει έστω μια φορά αριστερά το 17 θα γίνει αριστερό παιδί του 18 (όντως γιατί είναι μικρότερο από
18). Στην δεύτερη περίπτωση, διαγράφουμε το 16 και αντί για 17 έχουμε 19. Ο αμέσως επόμενος του 16 είναι το 18, επομένως 
πάμε μόνο δεξιά. Βάζουμε το 18 στη θέση του 16 και διαγράφουμε τον κόμβο που είχε το 18 και το 19 θα γίνει reattach ως δεξί 
παιδί.

### Ερώτημα 9

Στην άσκηση αυτή έπρεπε να αλλάξουμε την inorder και την Postorder που
εκτελούταν αναδρομικά και να τις κάνουμε με επανάληψη. Αρχικά, στην inorder
ελέγχω αν η ρίζα είναι NULL οπότε η συνάρτηση επιστρέφει. Στη συνέχεια, κάνω
malloc και initizalize τη stack, φτιάχνω έναν pointer σε treenode (CurrentNode)
που αρχικά δείχνει στη ρίζα και μια flag που αρχικά είναι 0 και για όσο είναι μηδέν,
αν ο CurrentNode δεν είναι NULL βάζει τον κόμβο αυτόν στη στοίβα και πηγαίνει
στο αριστερό υποδέντρο, αλλιώς αν η στοίβα δεν είναι κενή, κάνει Pop αυτό τον
κόμβο και τον εκτυπώνει και διασχίζει το δεξί υποδέντρο και συνεχίζει, αλλιώς αν η
στοίβα είναι άδεια κάνει την flag 1, οπότε βγαίνει από το while loop.

Στην Postorder κάνουμε τον ίδιο έλεγχο για το αν υπάρχει ρίζα και αν δεν υπάρχει η
συνάρτηση δεν έχει να κάνει κάτι οπότε επιστρέφει. Κάνουμε malloc και initialize
μια στοίβα, δημιουργούμε δυο pointers σε treenode, τον CurrentNode που αρχικά
δείχνει στη ρίζα του δέντρο και τον PreviousNode που αρχικά είναι NULL. Στη
συνέχεια, έχουμε ένα while loop που εκτελείται συνέχεια και μέσα έχουμε άλλο ένα
που εκτελείται όσο το CurrentNode δεν είναι NULL για να φτάσουμε στο πιο
αριστερό παιδί βάζοντας στη στοίβα όλα τα δεξιά παιδία και τις ρίζες. Αφού βγούμε
από το while κάνουμε Pop τον CurrentNode και αν έχει δεξί παιδί και η στοίβα δεν
είναι κενή βάζουμε τον επόμενο κόμβο που είναι στην κορυφή της στοίβας σε μια
μεταβλητή temp και τον ξαναβάζουμε στη στοίβα. Ελέγχουμε αν το δεξί παιδί του
CurrentNode είναι ο temp και αν είναι τον κάνουμε Pop τον temp και Push τον
CurrentNode πίσω στη στοίβα και πάμε στο δεξί παιδί και συνεχίζουμε προς τα
αριστερά. Αλλιώς αν δεν είναι ίσοι σημαίνει ότι είμαστε στο δεξί παιδί οπότε το
εκτυπώνουμε και κάνουμε το CurrentNode NULL. Αν δεν υπάρχει δεξί παιδί
εκτυπώνουμε τη ρίζα και κάνουμε το CurrentNode NULL. Τέλος, αν η στοίβα είναι
κενή κάνουμε break και βγαίνουμε από το while(1).

### Ερώτημα 10

Η συνάρτηση ClearTree παίρνει σαν όρισμα τη ρίζα του δέντρου και αρχικά ελέγχει
αν ο κόμβος που βρίσκεται είναι NULL, οπότε και επιστρέφει (base case). Στη
συνέχεια, ελέγχει αν υπάρχει αριστερό υποδέντρο και καλείται αναδρομικά με
όρισμα το αριστερό υποδέντρο και αντίστοιχα για το δεξί. Τέλος, κάνουμε free τον
κόμβο και η συνάρτηση επιστρέφει.

### Ερώτημα 11

Η συνάρτηση MaxDepth παίρνει σαν όρισμα την ρίζα ενός Binary Search Tree και
ελέγχει αν ο τρέχων κόμβος στον οποίο βρίσκεται είναι NULL οπότε επιστρέφει 0
αλλιώς υπολογίζει με αναδρομική κλήση το βάθος του αριστερού και στη συνέχεια
του δεξιού υποδέντρου. Ελέγχει ποιο από τα δυο είναι μεγαλύτερο και το βάζει στην
μεταβλητή depth, την οποία και επιστρέφει.

### Ερώτημα 12

Η συνάρτηση HasPathSum παίρνει σαν ορίσματα έναν δείκτη σε treenode και έναν
ακέραιο που έχει δώσει ο χρήστης και είναι το ζητούμενο άθροισμα Θέλουμε να
δούμε αν υπάρχει μονοπάτι στο δέντρο που το άθροισμα των entries των κόμβων
του μονοπατιού να είναι ίσο με τον αριθμό που έχει δώσει ο χρήστης. Ελέγχουμε αν
το δέντρο είναι κενό και στην περίπτωση αυτή η συνάρτηση επιστρέφει. Στη
συνέχεια, ελέγχουμε αν είμαστε σε φύλλο και επιστρέφουμε την τιμή της σύγκρισης
του sum με την τιμή του κόμβου που βρισκόμαστε. Αν επιστρέψει 1 σημαίνει ότι
υπάρχει μονοπάτι με το ζητούμενο άθροισμα. Ορίζω δυο μεταβλητές right και left
που τις χρησιμοποιώ για να αποθηκεύω το αποτέλεσμα της συνάρτησης σε κάθε
αναδρομική κλήση. Ελέγχουμε αν υπάρχει δεξί υποδέντρο, και αν ναι τότε καλούμε
αναδρομικά τη συνάρτηση με ορίσματα το δεξί υποδέντρο και το sum μείον την
τιμή του κόμβου που μόλις επισκεφτήκαμε. Αντίστοιχα κάνουμε το ίδιο και για το
αριστερό υποδέντρο και αποθηκεύουμε τα αποτελέσματα στις right και left
αντίστοιχα. Τέλος, ορίζω μια μεταβλητή ret, την οποία επιστρέφει η συνάρτηση, και
παίρνει σαν τιμή την τιμή της left ή της right μεταβλητής, αναλόγως του ποια είναι
μεγαλύτερη (δηλαδή ποια είναι 1).

## Άσκηση 3

### Ερώτημα 4

Για την υλοποίηση του αλγορίθμου Dijkstra, χρησιμοποίησα τον κώδικα για τις ουρές προτεραιότητας (την υλοποίηση με σωρό) και 
τους τύπους δεδομένων όπως είχαν οριστεί στην ενότητα για τους Γράφους. Στον κώδικα που αφορά τις ουρές προτεραιότητας έγιναν 
οι απαραίτητες τροποποιήσεις για να αφορά τα δεδομένα που θέλουμε να αποθηκεύσουμε στη συγκεκριμένη δομή. Συγκεκριμένα, στα 
typedefs όρισα το PQItem που έχει δύο πεδία, το πεδίο index που αποθηκεύεται ο αριθμός του κόμβου και το πεδίο distance που 
αποθηκεύεται το κόστος που χρειαζόμαστε για να φτάσουμε στον κόμβο, έτσι ώστε στην ουρά προτεραιότητας να κρατάμε και τις δυο 
πληροφορίες. Επιπλέον, αλλαγές έγιναν και στο implementation, όπου προστέθηκαν 3 συναρτήσεις: η swap που εκτελεί τις 
απαραίτητες αλλαγές μεταξύ των κόμβων, η heapify που μετά την ενημέρωση του πεδίου distance κάθε κόμβου θα πρέπει να 
επαναφέρει τη δομή σε σωρό και η συνάρτηση updatePriority που ενημερώνει τις αλλαγές στο κόστος. Τέλος, η υλοποίηση της 
ουράς προτεραιότητας στο συγκεκριμένο πρόβλημα έπρεπε να υλοποιεί MinHeap, οπότε πραγματοποιήθηκαν και οι αναγκαίες αλλαγές. 
Επίσης, στο αρχείο GraphTypes που περιέχει τα typedefs για τους Γράφους, στη struct Graph έχει προστεθεί ένα πεδίο που κρατάει 
το κόστος για να πάμε σε έναν κόμβο. Στο αρχείο DijkstraImplementation.c υλοποιείται ο αλγόριθμος. Αρχικοποιείται ο κόμβος 
αρχής σε (0,0) που σημαίνει ότι η απόσταση είναι μηδέν και ο πατέρας του είναι ο εαυτός του και όλοι οι άλλοι κόμβοι 
αρχικοποιούνται σε άπειρη απόσταση και με «κανέναν» ως πατέρα και τους τοποθετούμε στην ουρά προτεραιότητας. Στη συνέχεια, 
βγάζουμε τους γείτονες του τρέχοντος κόμβου και κάνουμε update το κόστος όπου χρειάζεται και διαλέγουμε την απόσταση με το 
μικρότερο κόστος. Ο αλγόριθμος συνεχίζει μέχρι να επισκεφτούμε όλους τους κόμβους του Γράφου.  Τέλος, στην main ο χρήστης 
καθορίζει τον αριθμό των κόμβων και των ακμών, τη σύνδεση και το κόστος μεταξύ των κόμβων και καλείται η συνάρτηση Dijkstra.

### Ερώτημα 6

Για την υλοποίηση του αλγορίθμου Kruskal υπάρχουν μέσα στον αντίστοιχο φάκελο 4 αρχεία. Το KruskalTypes.h περιέχει όλα τα 
structs που χρησιμοποιούνται στο πρόγραμμα. Το struct Edge που περιέχει όλη την πληροφορία για κάθε ακμή (από ποιον κόμβο 
ξεκινάει, σε ποιον καταλήγει και πόσο είναι το κόστος) και το struct Graph που περιέχει δυο πεδία για το σύνολο των κόμβων και 
των ακμών και ένα πεδίο struct Edge που περιέχει τις ακμές. Το επόμενο αρχείο, το KruskalInterfece.h, περιέχει τα πρωτότυπα 
όλων των συναρτήσεων που χρησιμοποιούνται από το πρόγραμμα και η υλοποίησή τους υπάρχει στο KruskalImplementation.c. Οι 
βοηθητικές συναρτήσεις είναι: 1) η EdgeComp που συγκρίνει τα βάρη των ακμών και χρησιμοποιήθηκε από την qsort, για την 
ταξινόμηση των ακμών με βάση το βάρος τους κατά αύξουσα σειρά, 2) η createGraph που καλείται από τη main για να κάνει malloc 
τον Γράφο και τον πίνακα που κρατάμε τις ακμές και επιστρέφει τον Γράφο, 3) η mostFrequentSet που τη χρησιμοποιούμε όταν 
θέλουμε να κάνουμε Union δυο σύνολα για να δούμε ποιο είναι μεγαλύτερο και να προσαρτήσουμε το μικρότερο σε αυτό. Παίρνει 
σαν ορίσματα τον πίνακα με το σύνολο που ανήκει ο κάθε κόμβος, τον κόμβο αρχής, τον κόμβο τέλους και τον αριθμό των κόμβων 
και διατρέχει τον πίνακα αυξάνοντας κάθε φορά έναν counter που αφορά είτε το ένα σύνολο είτε το άλλο και επιστρέφει το 
σύνολο το οποίο συνάντησε περισσότερες φορές. Και τέλος 4) η PrintVerticesSets που απλώς τυπώνει το περιεχόμενο του πίνακα 
vertexSets.

Η συνάρτηση KruskalMST παίρνει σαν όρισμα το Γράφο που δημιουργεί η main και αρχικά ταξινομεί τις ακμές κατά αύξουσα σειρά, 
δημιουργεί έναν πίνακα vertexSets, όπου αποθηκεύεται για κάθε κόμβο το σύνολο στο οποίο ανήκει (αρχικοποίηση κάθε κόμβου να 
είναι μόνος του ένα σύνολο με representative τον εαυτό του) και έναν πίνακα selectedEdges που παίρνει τιμές 0 και 1 ανάλογα 
με το αν έχουμε διαλέξει για το MST την εκάστοτε ακμή (αρχικοποίηση στο 0). Στη συνέχεια, με ένα for για όσες είναι οι ακμές 
συγκρίνουμε τα σύνολα που ανήκουν ο κόμβος src και ο κόμβος dest και αν διαφέρουν, κάνουμε 1 την selectedEdge[i], καλούμε την 
mostFrequentSet και αποθηκεύουμε την τιμή της στην μεταβλητή selectedSet (είναι το πιο συχνά εμφανιζόμενο σύνολο από τα δυο 
στα οποία ανήκουν οι δυο κόμβοι) και την τιμή του άλλου συνόλου την αποθηκεύουμε στην μεταβλητή setToReplace. Με ένα for για 
όσους κόμβους έχουμε διατρέχουμε τον πίνακα vertexSets και αλλάζουμε το μικρότερο σύνολο βάζοντας την τιμή του μεγαλύτερου. 
Αλλιώς, αν τα δυο σύνολα είναι ίδια αγνοούμε αυτή την ακμή και συνεχίζουμε. Στο τέλος, εκτυπώνουμε το MST και κάνουμε free 
τους δυο πίνακες (vertexSets και selectedEdges). Το τελευταίο αρχείο, Kruskalmain.c, καλεί την createGraph και στη συνέχεια 
δίνει τον αριθμό των ακμών και των κόμβων, καθώς και τις συνδέσεις και τα βάρη τους και καλεί την συνάρτηση KruskalMST, που 
υλοποιεί τον αλγόριθμο Kruskal.
