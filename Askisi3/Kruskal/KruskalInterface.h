#include "KruskalTypes.h"

void Union(struct subset subsets[], int x, int y);
int find(struct subset subsets[], int i);
int EdgeComp(const void* a, const void* b);
void KruskalMST(struct Graph* graph);
struct Graph* createGraph(int V, int E);
int mostFrequentSet(int *vertexSets, int source, int destination, int numV);
void PrintVerticesSets(int *vertexSets, int vertices);
