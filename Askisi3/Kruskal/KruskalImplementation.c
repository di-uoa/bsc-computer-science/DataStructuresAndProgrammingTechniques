#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "KruskalInterface.h"

void KruskalMST(struct Graph* graph)
{
    qsort(graph->edge, graph->E, sizeof(struct Edge ), EdgeComp);
    int *vertexSets;
    vertexSets = (int *)malloc(graph->V * sizeof(int));
    int i;
    for (i=0; i<graph->V; i++)
    {
        vertexSets[i] = i; //each vertex is a set
    }

    //this is an array to keep all the used edges in the MST
    int *selectedEdges;
    selectedEdges = (int*)malloc(graph->E * sizeof(int));
    //initialize the array to zero
    memset(selectedEdges, 0, graph->E * sizeof(int));

    for(i=0;i<graph->E;i++)
    {
        printf("\nLoop #%d\n",i);
        PrintVerticesSets(vertexSets, graph->V);
        int source = graph->edge[i].src;
        int destination = graph->edge[i].dest;
        //if the two vertices belong to different sets
        if(vertexSets[source] != vertexSets[destination])
        {
            selectedEdges[i] = 1;
            //we choose the most frequent set and we unite them
            int selectedSet = mostFrequentSet(vertexSets, source, destination, graph->V);
            int setToReplace = (selectedSet == vertexSets[source]) ? vertexSets[destination] : vertexSets[source];
            int j;
            printf("Searching in vertsets to replace %d with %d\n",setToReplace,selectedSet);
            for (j=0; j<graph->V; j++)
            {
                if (vertexSets[j] == setToReplace)
                    vertexSets[j] = selectedSet;
            }
            printf("Joined set %d to %d, over edge #%d :%d,%d,%d\n", setToReplace, selectedSet,i, graph->edge[i].src,graph->edge[i].dest,graph->edge[i].weight);


        }
        //else if the two vertices are on the same set, we ignore that edge
        else
        {
            printf("ignoring edge : %d %d %d\n", source, destination, graph->edge[i].weight);
        }
    }
    for(i=0;i<graph->E;i++)
    {
        if(selectedEdges[i] == 1)
        {
            printf("Edge # %d :  cost is %d with source vertex %d and destination vertex %d\n", i, graph->edge[i].weight, graph->edge[i].src, graph->edge[i].dest);
        }
    }

    free(vertexSets);
    free(selectedEdges);
}

// Compare two edges according to their weights.
// Used in qsort() for sorting an array of edges
int EdgeComp(const void* a, const void* b)
{
    struct Edge* a1 = (struct Edge*)a;
    struct Edge* b1 = (struct Edge*)b;
    return a1->weight > b1->weight;
}

// Creates a graph with V vertices and E edges
struct Graph* createGraph(int V, int E)
{
    struct Graph* graph = (struct Graph*) malloc( sizeof(struct Graph) );
    graph->V = V;
    graph->E = E;

    graph->edge = (struct Edge*) malloc( graph->E * sizeof( struct Edge ) );

    return graph;
}

int mostFrequentSet(int *vertexSets, int source, int destination, int numV)
{
    int countsource = 0;
    int countdestination = 0;
    int i;
    for (i=0;i<numV;i++)
    {
        if(vertexSets[i] == vertexSets[source])
        {
            countsource++;
        }
        else if (vertexSets[i] == vertexSets[destination])
        {
            countdestination++;
        }
    }
    if(countdestination > countsource)
        return vertexSets[destination];
    else
        return vertexSets[source];
}

void PrintVerticesSets(int *vertexSets, int vertices)
{
    printf("vertsets : ");
    int i;
    for(i=0; i<vertices;i++)
        printf("%d ", vertexSets[i]);
    printf("\n");
}
