/* This is the file "PQImplementation.c" */

#include "PQInterface.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void Initialize(PriorityQueue *PQ)
{
    PQ->Count=0;
}

int Empty(PriorityQueue *PQ)
{
    return(PQ->Count==0);
}

int Full(PriorityQueue *PQ)
{
    return(PQ->Count==MAXCOUNT);
}


void Insert(PQItem Item, PriorityQueue *PQ)
{
    int ChildLoc;
    int ParentLoc;

    (PQ->Count)++;
    ChildLoc=PQ->Count;
    ParentLoc=ChildLoc/2;
    while (ParentLoc != 0){
       if (Item.distance > PQ->ItemArray[ParentLoc].distance){
         PQ->ItemArray[ChildLoc]=Item;
         return;
       } else {
         PQ->ItemArray[ChildLoc]=PQ->ItemArray[ParentLoc];
         ChildLoc=ParentLoc;
         ParentLoc=ParentLoc/2;
       }
    }
    PQ->ItemArray[ChildLoc]=Item;
}


PQItem Remove(PriorityQueue *PQ)
{
   int CurrentLoc;
   int ChildLoc;
   PQItem ItemToPlace;
   PQItem ItemToReturn;

   if(Empty(PQ)) return ItemToReturn;

   ItemToReturn=PQ->ItemArray[1];
   ItemToPlace=PQ->ItemArray[PQ->Count];
   (PQ->Count)--;
   CurrentLoc=1; //current node
   ChildLoc=2*CurrentLoc; //left child of the current node

   while (ChildLoc <= PQ->Count){
       if (ChildLoc < PQ->Count){
          if (PQ->ItemArray[ChildLoc+1].distance < PQ->ItemArray[ChildLoc].distance){
             ChildLoc++;
          }
       }
       if (PQ->ItemArray[ChildLoc].distance >= ItemToPlace.distance){
           PQ->ItemArray[CurrentLoc]=ItemToPlace;
           return(ItemToReturn);
       } else {
           PQ->ItemArray[CurrentLoc]=PQ->ItemArray[ChildLoc];
           CurrentLoc=ChildLoc;
           ChildLoc=2*CurrentLoc;
       }
    }
    PQ->ItemArray[CurrentLoc]=ItemToPlace;

    return(ItemToReturn);
}

void updatePriority(int index,int newvalue,PriorityQueue * PQ)
{
    int i;
    for(i=0;i<PQ->Count;++i)
    {
        if(PQ->ItemArray[i+1].index==index)
        {

            PQ->ItemArray[i+1].distance = newvalue;
            printf("Before heapify:\n");print(PQ);
            heapify(&PQ, i+1);
            printf("After heapify:\n");print(PQ);

            return;
        }
    }
    printf("ERROR: Item %d - was not found in the PQ!\n",index);


}

void heapify(PriorityQueue **PQ, int i) {
    int index = i;
    while (index>0)
    {
        print(*PQ);
        int parent = floor(index/2);
        if ((*PQ)->ItemArray[index].distance < (*PQ)->ItemArray[parent].distance)
        {
            swap(&((*PQ)->ItemArray[index]), &((*PQ)->ItemArray[parent]));
        }
        index = parent;
    }
}

void swap(PQItem *i, PQItem *j)
{
    PQItem temp;
    temp.index = i->index;
    temp.distance = i->distance;
    i->distance = j->distance;
    i->index = j->index;
    j->index = temp.index;
    j->distance = temp.distance;
}

void print(PriorityQueue *Q)
{
    int i;
    for(i=0;i<Q->Count;++i)
        printf("(%d %d) ",Q->ItemArray[i+1].index,Q->ItemArray[i+1].distance);
    printf("\n");fflush(stdout);
}
