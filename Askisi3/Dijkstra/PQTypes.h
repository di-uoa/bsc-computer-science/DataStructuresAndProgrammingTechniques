/* This is the file PQTypes.h */

#define MAXCOUNT 10
typedef struct vert
{
    int index;
    int distance;
}PQItem;
typedef PQItem PQArray[MAXCOUNT+1];

typedef struct {
           int Count;
           PQArray ItemArray;
        } PriorityQueue;
