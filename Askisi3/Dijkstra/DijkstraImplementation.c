#include "PQInterface.h"
#include "DijkstraInterface.h"
#include <stdio.h>
#include <stdlib.h>

#define INFINITE 99999


void dijkstra(Graph G, int src)
{
    printf("Starting djikstra search, from node %d\n" ,src);fflush(stdout);
    int * distances = (int*)malloc(G.n  * sizeof(int));
    int * predecessor = (int*)malloc(G.n  * sizeof(int));
    int i;
    for(i=0;i<G.n;++i) distances[i] = 0;

    PriorityQueue PQ;
    predecessor[0] = 0;
    for(i=0;i<G.n;++i)
    {
        PQItem v;
        if(i != src)
        {
            distances[i] = INFINITE; // infinity
            predecessor[i] = 0; // undefined
        }
        v.distance = distances[i];
        v.index = i;
        Insert(v,&PQ);
    }

    while(!Empty(&PQ))
    {
        // extract min
        print(&PQ);
        PQItem v = Remove(&PQ);

        printf("Selected from PQ node  %d with dist %d \n",v.index,v.distance);fflush(stdout);
        print(&PQ);
        // for each neighbour

        int neighbourCount = 0;
        Edge * e = G.firstedge[v.index]->nextedge;
        while(e != NULL)
        {
            int neighbourVertex = e->endpoint;
            printf("Selected neighbour  of %d : %d with dist %d \n",v.index,neighbourVertex,distances[neighbourVertex]); fflush(stdout);
            printf("cost of edge : %d, curr dist of %d : %d\n",G.cost[v.index][neighbourVertex],distances[neighbourVertex]); fflush(stdout);
            int updcost = G.cost[v.index][neighbourVertex] + distances[v.index];
            if(updcost < distances[neighbourVertex])
            {
                distances[neighbourVertex] = updcost;
                predecessor[neighbourVertex] = v.index;

                updatePriority(neighbourVertex,updcost,&PQ);
            }

            e = e->nextedge;
            neighbourCount++;
        }

    }

    printf("Djikstra algorithm result:\n");
    for(i=0;i<G.n;++i)
    {
        printf("Node: %d\tcost:%d\tpredecessor:%d\n",i,distances[i],predecessor[i]);
    }

    return;
}


/*

1  function Dijkstra(Graph, source):
2      dist[source] ← 0                                    // Initialization
3
4      create vertex set Q
5
6      for each vertex v in Graph:
7          if v ≠ source
8              dist[v] ← INFINITY                          // Unknown distance from source to v
9              prev[v] ← UNDEFINED                         // Predecessor of v
10
11         Q.add_with_priority(v, dist[v])
12
13
14     while Q is not empty:                              // The main loop
15         u ← Q.extract_min()                            // Remove and return best vertex
16         for each neighbor v of u:                       // only v that is still in Q
17             alt = dist[u] + length(u, v)
18             if alt < dist[v]
19                 dist[v] ← alt
20                 prev[v] ← u
21                 Q.decrease_priority(v, alt)
22
23     return dist[], prev[]



* */
