#include <stdio.h>
#include <stdlib.h>
#include "DijkstraInterface.h"
int main(void)
{

    Graph G;
    G.n = 5;
    int i;
    for(i=0;i<G.n;i++)
    {
        G.firstedge[i] = (Edge*) malloc(sizeof (Edge));
        G.firstedge[i]->endpoint = i;
    }

    // set the rest of nodes to null
    for(i=G.n;i<MAXVERTEX;i++)
    {
        G.firstedge[i] = NULL;
    }
    Edge *ptr;
    int vertex1, vertex2;

    vertex1 = 0;
    vertex2 = 1;
    ptr = G.firstedge[vertex1];
    while(ptr->nextedge != NULL)
        ptr = ptr->nextedge;
    ptr->nextedge = (Edge*)malloc(sizeof(Edge)); ptr=ptr->nextedge;ptr->endpoint=vertex2;ptr->nextedge=NULL;
    G.cost[vertex1][vertex2] = 1;

    vertex1 = 1;
    vertex2 = 2;
    ptr = G.firstedge[vertex1];
    while(ptr->nextedge != NULL)
        ptr = ptr->nextedge;
    ptr->nextedge = (Edge*)malloc(sizeof(Edge)); ptr=ptr->nextedge;ptr->endpoint=vertex2;ptr->nextedge=NULL;
    G.cost[vertex1][vertex2] = 2;

    vertex1 = 1;
    vertex2 = 3;
    ptr = G.firstedge[vertex1];
    while(ptr->nextedge != NULL)
        ptr = ptr->nextedge;
    ptr->nextedge = (Edge*)malloc(sizeof(Edge)); ptr=ptr->nextedge;ptr->endpoint=vertex2;ptr->nextedge=NULL;
    G.cost[vertex1][vertex2] = 1;

    vertex1 = 2;
    vertex2 = 4;
    ptr = G.firstedge[vertex1];
    while(ptr->nextedge != NULL)
        ptr = ptr->nextedge;
    ptr->nextedge = (Edge*)malloc(sizeof(Edge)); ptr=ptr->nextedge;ptr->endpoint=vertex2; ptr->nextedge=NULL;
    G.cost[vertex1][vertex2] = 2;

    vertex1 = 3;
    vertex2 = 4;
    ptr = G.firstedge[vertex1];
    while(ptr->nextedge != NULL)
        ptr = ptr->nextedge;
    ptr->nextedge = (Edge*)malloc(sizeof(Edge)); ptr=ptr->nextedge;ptr->endpoint=vertex2; ptr->nextedge=NULL;
    G.cost[vertex1][vertex2] = 1;

    int src = 0;
    dijkstra(G, src);


    return 6;

    /*
    // populate the graph
    int numEdges;

       // enter num. vertices
    printf("Give number of vertices\n");
    scanf("%d", &(G.n));
       // enter num. edges2
    printf("Give number of edges\n");
    scanf("%d", &numEdges);
    int i;

    for(i=0;i<G.n;i++)
    {
        G.firstedge[i] = (Edge*) malloc(sizeof (Edge));
        G.firstedge[i]->endpoint = i;
    }

    // set the rest of nodes to null
    for(i=G.n;i<MAXVERTEX;i++)
    {
        G.firstedge[i] = NULL;
    }
   for(i=0;i<numEdges;++i)
   {
        int vertex1, vertex2, cost1;

       // enter vertex1, vertex2

        printf("Give pairs of vertices between 0 and %d \n", G.n-1);
        scanf ("%d", &vertex1);
        scanf("%d", &vertex2);
        printf("Give the cost of the edge:\n");
        scanf("%d", &cost1);

        //check if the values of vertices are correct
        if (vertex1>=G.n || vertex2>=G.n || vertex1<0)
        {
            printf("Wrong pairs\n");
            i--;
            continue;
        }
        //connecting the two vertices
        Edge *ptr;
        ptr = G.firstedge[vertex1];
        while(ptr->nextedge != NULL)
            ptr = ptr->nextedge;
        ptr->nextedge = G.firstedge[vertex2];
        G.cost[vertex1][vertex2] = cost1;
        //G.firstedge[vertex1]->nextedge = G.firstedge[vertex2];

   }
    printf("Give a number between 0 and %d for the starting vertex:\n", G.n-1);
    int src;
    scanf("%d", &src);
    dijkstra(G, src);
    return 0;   */
}
