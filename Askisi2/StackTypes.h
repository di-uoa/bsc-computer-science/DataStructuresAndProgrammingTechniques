/* This is the file StackTypes.h   */

typedef struct treenode TreeNode; //I want to put pointer to treenodes into the stack


/* char is the type for our first application */
/* float is the type for our second application */

typedef struct StackNodeTag {
           TreeNode *Item;
           struct StackNodeTag *Link;
        } StackNode;

typedef struct {
           StackNode *ItemList;
        } Stack;
